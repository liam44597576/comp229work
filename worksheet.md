# Task 0

Clone this repository (well done!)

# Task 1

Take a look a the two repositories:
  
  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A? farleyknight
  * Who made the first commit to repository A? scott Chacon
  * Who made the last and first commits to repository B? the first commit was from the Android
  Open Source Project
  the last commit was from Shawn O.Pearce
  * Who has been the most active recent contributor on repository A?  How about repository B?
  Repository A's most active recent contributor is Joshua Nichols
  Repository B's most active recent contributor is Shawn O.Pearce
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
  Both of the projects have not been updated since at least 2013 (2012 for project B), based on this we can assume that neither project is active at the moment. Project A appears to have been completed, hence has not required further commits 
  Project B appears to have been on the verge of completion, however it looks as though the project has been either left unfinished or buggy, as many of the recent commits are not polishing commits, but rather fixes for bugs
  * 🤔 Which file in each project has had the most activity?
  Project A  : lib.rb
  Project B  : project.py

# Task 2

Setup a new eclipse project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~ 

🤔 Now setup a new bitbucket repository and have this project pushed to that repository.  You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore` file.  The one we have in this repository is a very good start.