import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.event.*;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.Timer;

import java.awt.PointerInfo;
import java.awt.Point;


public class Practical2 extends JPanel implements ActionListener
{
	//Opted to use ActionListner to perform repaint() function
	//Use of the ActionListner for repaint function sourced from here:
	//http://stackoverflow.com/questions/11129608/using-repaint-method-with-actionperformed
	//Use of timer to repaint every x time found here:
	//http://stackoverflow.com/questions/22072796/how-to-repaint-a-jpanel-every-x-seconds
	public static Grid grid;
	private Timer anim;	//Use of timer makes repaint() function work
	
	//establishing grid object and timer for repaint()
	public Practical2(){
		grid = new Grid(this);
		this.anim = new Timer(10, this);
		this.anim.start();
		
	}
	@Override
	public void paint(Graphics g){
		grid.paint(g);;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0){
		repaint();
	}
	
	//Grid class that subsumes Cell array
	static class Grid {
		static Cell[][] GridPoint = new Cell[20][20];
		
		//creating grid of cells using nested loops
		Grid(JPanel panel){
			for(int i = 0; i < 20; i++){
				for(int j = 0; j < 20; j++){
					int x = 10 + j*35;
					int y = 10 + i*35;
					GridPoint[i][j] = new Cell(x,y,j*i,panel);
				}
			}
		}
		
		public void paint(Graphics g){
			for(int i = 0; i<20; i++){
				for(int j = 0; j<20; j++){
					GridPoint[j][i].clear(g);	//clear panel
					GridPoint[j][i].mouse();	//get mouse position
					GridPoint[j][i].paint(g);	//paint panel again
				}
			}
		}
	}
	
	//Cell class that represents the drawn grid, allowing for cell size, calling
	//of cells, clearing of panel, mouse position info, and panel painting
	static class Cell
	{
		int size = 35;
		int x = 0;
		int y = 0;
		int num = -1;
		boolean select = false;
		JPanel contain;
		
		//establishing cell data information, x and y setters eg
		Cell(int x, int y, int num, JPanel cont){
			this.x = x;
			this.y = y;
			this.num = num;
			this.contain = cont;
			
		}
		
		public void setx(int x){
			this.x = x;
		}
		
		public void sety(int y){
			this.y = y;
		}
		
		//clear panel of previous paint
		public void clear(Graphics g){
			Graphics2D g2 = (Graphics2D)g;
			g2.setColor(this.contain.getBackground());
			g2.fillRect(this.x, this.y, this.size, this.size);
		}
		
		//get mouse position info, if mouse position is over a cell, enable
		//cell selected bool, else do nothing
		public void mouse(){
			PointerInfo mouseps = MouseInfo.getPointerInfo();
			Point posi = mouseps.getLocation();
			int x = (int) posi.getX();
			int y = (int) posi.getY();
			y= y-48;
			if(x>this.x && x<= this.x + this.size && y>this.y && y<= this.y + this.size){
				this.select = true;
			}else{
				this.select = false;
			}
		}
		
		//if select bool is true, colour the selected cell, else do not colour
		//the cell
		public void paint(Graphics g){
			Graphics2D g2 = (Graphics2D)g;
			
			if(this.select == true){
				g2.setColor(Color.gray);
				g2.fillRect(this.x, this.y, this.size, this.size);
			}else{
				g2.setColor(Color.lightGray);
				g2.drawRect(this.x, this.y, this.size, this.size);
				
			}
		}
	}
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Practical2");
		frame.setSize(1280, 720);
		Practical2 p = new Practical2();
		frame.add(p);
		frame.setVisible(true);
		
	}
	}

	

